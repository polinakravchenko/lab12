﻿// lab12.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

class Mobile1
{
private:

    string model;
    int memory;
    int year;
    double cost;
    int number;

public:

    Mobile1()
    {

        model = "Sumsung A30";
        memory = 64;
        year = 2019;
        cost = 283;
        number = 544674;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "The memory of phone: " << memory << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};
class Mobile2
{
private:

    string model;
    int memory;
    int year;
    double cost;
    int number;

public:

    Mobile2()
    {

        model = "Xiamomi 11T";
        memory = 128;
        year = 2021;
        cost = 600;
        number = 318527;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "The memory of phone: " << memory << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};

class Mobile3
{
private:

    string model;
    int memory;
    int year;
    double cost;
    int number;

public:

    Mobile3()
    {

        model = "Iphone 13ProMax";
        memory = 256;
        year = 2021;
        cost = 1400;
        number = 318463;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "The memory of phone: " << memory << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};

class Mobile4
{
private:

    string model;
    int memory;
    int year;
    double cost;
    int number;

public:

    Mobile4()
    {

        model = "Samsung Flip3 8";
        memory = 128;
        year = 2021;
        cost = 1000;
        number = 310630;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "The memory of phone: " << memory << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};

class Mobile5
{
private:

    string model;
    int memory;
    int year;
    double cost;
    int number;

public:

    Mobile5()
    {

        model = "Xiaomi Redmi Note 11";
        memory = 128;
        year = 2021;
        cost = 430;
        number = 33413;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "The memory of phone: " << memory << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};

int main()
{
    Mobile1 Tony;
    Tony.print_info();

    Mobile2 Jack;
    Jack.print_info();

    Mobile3 Hary;
    Hary.print_info();

    Mobile4 Elise;
    Elise.print_info();

    Mobile5 Lusy;
    Lusy.print_info();


    const int cost = 5;
    int array[cost] = { 283, 600, 1400, 1000, 430};
    int i;
    int summa = 0;

    for (i = 0; i < 10; i++)
        summa += array[i];
    

    const int memory = 5;
    int arr[memory] = { 64,128,256,128,128};
    int min = arr[0];
    int max = arr[0];
    for (int i = 0; i <memory; i++)
    {

        if (arr[i] > max)
        {
            max = arr[i];
        }
    }
    std::cout << "Average cost of phones: " << summa / 5 << " Dollars" << std::endl;
    std::cout << "Max memory of the phone: " << max << " GB" << std::endl;
    std::cout << "Total: 4 phones was created in 2021" << std::endl;
    return 0;
}